<%-- 
    Document   : dictionary
    Created on : 21.12.2015, 22:23:20
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dictionary Page</title>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    </head>
    <body>
        <h3>Use this page to manage your phrasal verbs</h3>
        <p onClick="getAllTranslates()">Show all</p>
        <p onclick="viewCreatingTools()">Create new one</p>
        <div id="feedback"></div>

        <script type="text/javascript">
            function getAllTranslates() {
                var request = "showAll";
                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    url: "getAllTranslates",
                    data: request,
                    dataType: 'json',
                    success: function (data) {
                        //viewAllTranslates(data)
                        console.log("Obtain: ", data)
                        viewAllTranslates(data)
                    }
                });
            }
            function viewAllTranslates(data) {
                var div = document.createElement('div');
                div.className = 'all_translates';
                var table = document.createElement('table');
                table.border = 1;
                table.id = 'table_all_translates';
                var header = table.createTHead();
                var headerRow = header.insertRow(0);
                var headerCell = headerRow.insertCell(0);
                headerCell.innerHTML = "<p>prefix</p>";
                headerCell = headerRow.insertCell(1);
                headerCell.innerHTML = "<p>verb</p>";
                headerCell = headerRow.insertCell(2);
                headerCell.innerHTML = "<p>translate</p>";
                headerCell = headerRow.insertCell(3);
                headerCell.innerHTML = "<p>shows</p>";
                headerCell = headerRow.insertCell(4);
                headerCell.innerHTML = "<p>hints</p>";
                headerCell = headerRow.insertCell(0);
                headerCell.innerHTML = "<p>id</p>";
                var tbody = document.createElement("tbody");
                for (var i = 0; i < data.length; i++) {
                    var row = tbody.insertRow();
                    row.setAttribute("id",data[i].idTranslate);
                    var cell = row.insertCell();
                    cell.innerHTML = data[i].idTranslate;
                    cell = row.insertCell();
                    cell.innerHTML = data[i].prefix;
                    cell = row.insertCell();
                    cell.innerHTML = data[i].verb;
                    cell = row.insertCell();
                    cell.innerHTML = data[i].title;
                    cell = row.insertCell();
                    cell.innerHTML = data[i].shows;
                    cell = row.insertCell();
                    cell.innerHTML = data[i].hints;
                    cell = row.insertCell();
                    var editButton = document.createElement('input');
                    editButton.setAttribute("type","button");
                    editButton.setAttribute("value","Edit");
                    editButton.setAttribute("onclick", "translateEditor("+i+");");
                    cell.appendChild(editButton);
                    cell = row.insertCell();
                    var deleteButton = document.createElement('input');
                    deleteButton.setAttribute("type","button");
                    deleteButton.setAttribute('value','Delete');
                    deleteButton.setAttribute('onclick',"confirmDelete("+i+");");
                    cell.appendChild(deleteButton);
                }
                table.appendChild(tbody);
                document.body.appendChild(table);
            }
            function translateEditor(id){
                id++;
                var t = document.getElementById("table_all_translates");
                //console.log("line", t.rows[id]);
                //t.rows[id].innerHTML="asdf";
                
                for(var i = 0; i<t.rows[id].cells.length -4;i++){
                    var cell = t.rows[id].cells[i];
                    var textbox = document.createElement('input');
                    textbox.setAttribute("type", "text");
                    textbox.setAttribute("value", t.rows[id].cells[i].innerText);
                    textbox.setAttribute("onKeyDown","this.size = this.value.length;");
                    //textbox.style.width= 20 + 'px';
                    textbox.size = t.rows[id].cells[i].innerText.length;
                    cell.innerHTML = "";
                    cell.appendChild(textbox);                                       
                }
                
                    var cell = t.rows[id].cells[t.rows[id].cells.length-2];
                    var OKbutton = document.createElement('input');
                    OKbutton.setAttribute("type","button");
                    OKbutton.setAttribute("value", "OK");
                    OKbutton.setAttribute("onclick","acceptChanges("+id+");")
                    cell.innerHTML="";
                    cell.appendChild(OKbutton);
                    var cell = t.rows[id].cells[t.rows[id].cells.length-1];
                    var CancelButton = document.createElement('input');
                    CancelButton.setAttribute("type","button");
                    CancelButton.setAttribute("value", "Cancel");
                    CancelButton.setAttribute("onclick","hideEditor("+id+");")
                    cell.innerHTML="";
                    cell.appendChild(CancelButton);
                
            }
            function acceptChanges(id){
                var table = document.getElementById("table_all_translates");
                var data = {};
                var fieldNames = ["idTranslate","prefix","verb","title","usecase"];
                for(var i = 0; i < table.rows[id].cells.length - 4;i++){
                    var cell = table.rows[id].cells[i];
                    var text = cell.getElementsByTagName('input')[0].value;
                    data[fieldNames[i]] = text;
                }
                
                    $.ajax({
                        type: "POST",
                        contentType: "application/json",
                        url: "editTranslate",
                        data: JSON.stringify(data),
                        dataType: "json",
                        success: function (data){
                            console.log("obtain: ", data);
                        }
                    });
            }
            function confirmDelete(id){
                
            }
            function addTranslate() {
                var data = {}
                data['verb'] = document.getElementById('verb').value;
                data["prefix"] = document.getElementById('prefix').value;
                data["translate"] = document.getElementById('translate').value;
                data["example"] = document.getElementById('example').value;

                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    url: "createTranslate",
                    data: JSON.stringify(data),
                    dataType: 'json',
                    timeout: 100000,
                    success: function (data) {
                        console.log("SUCCESS: ", data);
                        display(data);
                    },
                    error: function (e) {
                        console.log("ERROR: ", e);
                        display(e);
                    },
                    done: function (e) {
                        console.log("DONE");
                        //enableSearchButton(true);
                    }
                });
            }

            function display(data) {
                var json = "<h4>Ajax Response</h4><pre>"
                        + JSON.stringify(data, null, 4) + "</pre>";
                $('#feedback').html(json);
            }

            function viewCreatingTools() {
                var div = document.createElement('div');
                div.className = "newVerb";
                var form = document.createElement('form');
                form.method = 'POST';
                form.action = "reateTranslate";
                form.id = "add-form";
                //form.setAttribute("enctype", "multipart/form-data");                
                //form.setAttribute("encoding", "multipart/form-data");
                var labelForTitle = document.createElement('label');
                labelForTitle.innerHTML = "Input verb's title";
                form.appendChild(labelForTitle);
                var inputForTitle = document.createElement("input");
                inputForTitle.setAttribute("type", "text");
                inputForTitle.id = "verb";
                form.appendChild(inputForTitle);

                var labelForPrefix = document.createElement('label');
                labelForPrefix.innerHTML = "Input prefix";
                form.appendChild(labelForPrefix);
                var inputForPrefix = document.createElement('input');
                inputForPrefix.setAttribute("type", "text");
                inputForPrefix.id = "prefix";
                form.appendChild(inputForPrefix);

                var labelForTranslate = document.createElement('label');
                labelForTranslate.innerHTML = "Input Translate";
                form.appendChild(labelForTranslate);
                var inputForTranslate = document.createElement('input');
                inputForTranslate.setAttribute("type", "text");
                inputForTranslate.id = "translate";
                form.appendChild(inputForTranslate);

                var labelForExample = document.createElement('label');
                labelForExample.innerHTML = "Input Example";
                form.appendChild(labelForExample);
                var inputForExample = document.createElement('input');
                inputForExample.setAttribute("type", "text");
                inputForExample.id = "example";
                form.appendChild(inputForExample);

                var button = document.createElement('input');
                button.setAttribute("type", "button");
                button.setAttribute("value", "Add");
                button.setAttribute('onclick', 'addTranslate();');
                form.appendChild(button);
                document.body.appendChild(form);

            }
        </script>
    </body>
</html>
