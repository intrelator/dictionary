<%-- 
    Document   : index
    Created on : 14.12.2015, 21:44:02
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello!</h1>
        <h5>This is a phrasal verb dictionary</h5>
        <h6>Click Start to training</h6>
        <a href="<c:url value="/training" />">Start</a>
        <a href="<c:url value="/dictionary"/>">Dictionary</a>
    </body>
</html>
