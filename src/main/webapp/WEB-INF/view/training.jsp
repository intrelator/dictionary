<%-- 
    Document   : training
    Created on : 15.12.2015, 20:10:48
    Author     : root
--%>

<%@page import="Entities.Translate"%>
<%@page import="Entities.Prefix"%>
<%@page import="Entities.Verb"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <title>JSP Page</title>
    </head>

    <body>
        <h1>Training</h1>
        <div style="border: 1px solid black">
            <span>Select verbs</span>  
            <c:forEach var="verb" items="${requestScope.verbs}">
                <p><input  name="verb" id="firstcb" type="checkbox" value="${verb.getIdVerb()}"/>${verb.getTitle()}</p>
                </c:forEach>
        </div>
        <div style="border: 1px solid black">
            <span>Select prefixes</span>     
            <c:forEach var="p" items="${requestScope.prefixes}">
                       <p><input name ="prefix" id="firstcb" type="checkbox" value="${p.getIdPrefix()}"/>${p.getTitle()}</p>
            </c:forEach>  
    </div>
    <input type="button" value="Start" onclick="countCheckbox()">

    <script type="text/javascript">
        function textToSpeech(text) {
            var msg = new SpeechSynthesisUtterance(text);
            window.speechSynthesis.speak(msg);
        }
        function countCheckbox() {
            var checkbox;
            checkbox = document.getElementsByName("verb"); // получаем массив чекбоксов
            var verbIds = "";
            for (var i = 0; i < checkbox.length; i++) {// пробегаем весь массив
                if (checkbox[i].checked)
                    verbIds += checkbox[i].value + ";"; // записываем каждое значение в строку и разделяем пробелом
            }
            var prefixIds = "";
            checkbox = document.getElementsByName("prefix"); // получаем массив чекбоксов
            for (var i = 0; i < checkbox.length; i++) {// пробегаем весь массив
                if (checkbox[i].checked)
                    prefixIds += checkbox[i].value + ";"; // записываем каждое значение в строку и разделяем пробелом
            }
            $.ajax({
                type: "POST",
                url: "setVerbsAndPrefix",
                data: "verbIds=" + verbIds + "&prefixIds=" + prefixIds,
                success: function (msg) {
                    show(msg);
                    //alert("Прибыли данные: " + msg.verb + "   " + msg.prefix);
                }
            });
        }
        function show(msg) {
            var hiddenText = document.createElement("p");
            hiddenText.innerHTML = msg.verb + ", " + msg.prefix;
            hiddenText.setAttribute("style", "display:none;");
            hiddenText.setAttribute("id", "textToSpeech");
            var div = document.createElement('div');
            div.className = "task";
            div.id = "task";
            div.innerHTML = "Перевод " + msg.verb + "   " + msg.prefix + " это:<br>" +
                    "<input type=\"radio\" onclick=\"sendAnswer()\" name=\"answer\" value=\"" + msg.translatesId[0] + "\" /> " + msg.translates[0] +
                    "<input type=\"radio\" onclick=\"sendAnswer()\" name=\"answer\" value=\"" + msg.translatesId[1] + "\" /> " + msg.translates[1] +
                    "<input type=\"radio\" onclick=\"sendAnswer()\" name=\"answer\" value=\"" + msg.translatesId[2] + "\" /> " + msg.translates[2] +
                    "<input type=\"radio\" onclick=\"sendAnswer()\" name=\"answer\" value=\"" + msg.translatesId[3] + "\" /> " + msg.translates[3];

            var parentElem = document.body;
            parentElem.appendChild(hiddenText);
            parentElem.appendChild(div);
        }
        function sendAnswer() {
            //var text = document.getElementById("textToSpeech");
            //textToSpeach(text.innerText);
            var radioButtons = document.getElementsByName("answer");
            var answer = {}
            for (var i = 0; i < radioButtons.length; i++) {
                if (radioButtons[i].checked) {
                    answer["answer"] = radioButtons[i].value;
                    break;
                }
            }
            $.ajax({
                type: "POST",
                url: "getNewQuestion",
                dataType: "json",
                contentType: 'application/json',
                data: JSON.stringify(answer),
                success: function (data) {
                    console.log("obtain message ", data);
                    showNewQuestion(data);
                }
            });
        }
        function showNewQuestion(msg) {
            var button = document.createElement("input");
            button.type = "button";
            button.value = "Finish training";
            button.addEventListener("click", stopTraining);
            var task = document.getElementById("task");
            var color = "";
            if (msg.result)
                color = "green";
            else
                color = "red";
            task.innerHTML = "<br><div style=\"padding:10px 10px\;background:" + color + "\"></div>" +
                    "Перевод " + msg.verb + "   " + msg.prefix + " это:<br>" +
                    "<input type=\"radio\" onclick=\"sendAnswer()\" name=\"answer\" value=\"" + msg.translatesId[0] + "\" > " + msg.translates[0] + "</>" +
                    "<input type=\"radio\" onclick=\"sendAnswer()\" name=\"answer\" value=\"" + msg.translatesId[1] + "\" > " + msg.translates[1] + "</>" +
                    "<input type=\"radio\" onclick=\"sendAnswer()\" name=\"answer\" value=\"" + msg.translatesId[2] + "\" > " + msg.translates[2] + "</>" +
                    "<input type=\"radio\" onclick=\"sendAnswer()\" name=\"answer\" value=\"" + msg.translatesId[3] + "\" > " + msg.translates[3] + "</>";
            task.appendChild(button);
            var parentElem = document.body;
            parentElem.appendChild(task);
        }
        function stopTraining() {
            $.ajax({
                type: "POST",
                url: "stopTraining",
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    console.log("obtain :", data);
                    showTrainingResult(data);
                }
            })
        }
        function showTrainingResult(data) {
            var task = document.getElementById("task");
            task.innerHTML = "<p>Всего кобинаций: " + data.combinationsAviable + "</p>" +
                    "<p>Вопросов: " + data.shows + "</p>" +
                    "<p>Правильных ответов: " + data.hints + "</p>";
            var parent = document.body;
            parent.appendChild(task);
        }
    </script>
</body>
</html>



