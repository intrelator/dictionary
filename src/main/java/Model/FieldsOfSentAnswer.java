/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author root
 */
public class FieldsOfSentAnswer {
    int verbId;
    int pefixId;
    int translateId;

    public int getVerbId() {
        return verbId;
    }

    public void setVerbId(int verbId) {
        this.verbId = verbId;
    }

    public int getPefixId() {
        return pefixId;
    }

    public void setPefixId(int pefixId) {
        this.pefixId = pefixId;
    }

    public int getTranslateId() {
        return translateId;
    }

    public void setTranslateId(int translateId) {
        this.translateId = translateId;
    }
    
}
