/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author root
 */
public class RespForShowResult {
    private int shows;
    private int hints;
    private int combinationsAviable;

    public int getShows() {
        return shows;
    }

    public void setShows(int shows) {
        this.shows = shows;
    }

    public int getHints() {
        return hints;
    }

    public void setHints(int hints) {
        this.hints = hints;
    }

    public int getCombinationsAviable() {
        return combinationsAviable;
    }

    public void setCombinationsAviable(int combinationsAviable) {
        this.combinationsAviable = combinationsAviable;
    }
    
}
