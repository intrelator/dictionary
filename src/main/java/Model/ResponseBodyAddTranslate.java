/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import com.fasterxml.jackson.annotation.JsonView;
import jsonView.Views;

/**
 *
 * @author root
 */
public class ResponseBodyAddTranslate {
    @JsonView(Views.Public.class)
    String message;
//    @JsonView(Views.Public.class)
//    String verb;
//    @JsonView(Views.Public.class)
//    String prefix;
//    @JsonView(Views.Public.class)
//    String translate;
//    @JsonView(Views.Public.class)
//    String example;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
}
