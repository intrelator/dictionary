/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author root
 */
public class RespForGetQuestion {
    
    private String verb;    
    private String prefix;
    private String[] translates;
    private String useCase;
    private boolean result;
    private int[] translatesId;
    private int shows;
    private int hints;
    private int combinationsAviable;

    public int[] getTranslatesId() {
        return translatesId;
    }

    public void setTranslatesId(int[] translatesId) {
        this.translatesId = translatesId;
    }

    public int getShows() {
        return shows;
    }

    public void setShows(int shows) {
        this.shows = shows;
    }

    public int getHints() {
        return hints;
    }

    public void setHints(int hints) {
        this.hints = hints;
    }

    public int getCombinationsAviable() {
        return combinationsAviable;
    }

    public void setCombinationsAviable(int combinationsAviable) {
        this.combinationsAviable = combinationsAviable;
    }
    
    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
    
    public void setVerb(String verb) {
        this.verb = verb;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setTranslates(String[] translates) {
        this.translates = translates;
    }

    public void setUseCase(String useCase) {
        this.useCase = useCase;
    }

    public String getVerb() {
        return verb;
    }

    public String getPrefix() {
        return prefix;
    }

    public String[] getTranslates() {
        return translates;
    }

    public String getUseCase() {
        return useCase;
    }
    

    

    
    
}
