/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.Interfaces.PrefixInterface;
import Entities.Prefix;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author root
 */
public class PrefixImpl implements PrefixInterface {    
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void add(Prefix p) {
        Session s = sessionFactory.openSession();
        s.beginTransaction();
        s.save(p);
        s.getTransaction().commit();
        s.close();
    }

    @Override
    public List<Prefix> getAll() {
        Session session = sessionFactory.openSession();
        List<Prefix> prefixes = session.createCriteria(Prefix.class).list();
        return prefixes;
    }

    @Override
    public void remove(Prefix p) {
        Session s = sessionFactory.openSession();
        s.beginTransaction();
        s.delete(p);
        s.getTransaction().commit();
        s.close();
    }

    @Override
    public void update(Prefix p) {
        Session s = sessionFactory.openSession();
        s.beginTransaction();
        s.update(p);
        s.getTransaction().commit();
        s.close();
    }
    @Override
    public Prefix getByTitle(String t) {
        Prefix result = null;
        List<Prefix> list;
        Session session = sessionFactory.openSession();
        list = session.createCriteria(Prefix.class).add(Restrictions.eq("title", t)).list();
        if (list.size()>0)
            result = list.get(0);
        session.close();
        return result;
    }

    @Override
    public Prefix getPrefixWithJoins(int id) {
        Session s = sessionFactory.openSession();        
        Prefix p = (Prefix) s.get(Prefix.class, id);
        Hibernate.initialize(p.getTranslates());
        return p;
    }
    
}
