/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.Interfaces.VerbInterface;
import Entities.Verb;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author root
 */
public class VerbImpl implements VerbInterface {
    
    private SessionFactory sessionFactory;    

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void add(Verb v) {
        Session s = sessionFactory.openSession();
        s.beginTransaction();
        s.save(v);
        s.getTransaction().commit();
        s.close();
    }

    @Override
    public List<Verb> getAll() {
        Session s = sessionFactory.openSession();
        List<Verb> verbs = s.createCriteria(Verb.class).list();
        s.close();
        return verbs;
    }

    @Override
    public void remove(Verb v) {
        Session s = sessionFactory.openSession();
        s.beginTransaction();
        s.delete(v);
        s.getTransaction().commit();
        s.close();
    }

    @Override
    public void update(Verb v) {
        Session s = sessionFactory.openSession();
        s.beginTransaction();
        s.update(v);
        s.getTransaction().commit();
        s.close();
    }

    @Override
    public Verb getByTitle(String t) {
        Verb result = null;
        List<Verb> list;
        Session session = sessionFactory.openSession();
        list = session.createCriteria(Verb.class).add(Restrictions.eq("title", t)).list();
        if (list.size()>0)
            result = list.get(0);
        session.close();
        return result;
    }

    @Override
    public Verb getVerbWithJoins(int id) {
    Session s = sessionFactory.openSession();
    Verb v = (Verb)s.get(Verb.class, id);
    Hibernate.initialize(v.getTranslates());
    s.close();
    return v;
    }
    
    
}
