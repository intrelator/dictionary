/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Translate;
import DAO.Interfaces.TranslateInterface;
import Entities.Prefix;
import Entities.Verb;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author root
 */
public class TranslateImpl implements TranslateInterface {

    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void add(Translate t) {
        Session s = sessionFactory.openSession();
        s.save(t);
        s.close();
    }

    @Override
    public List<Translate> getAll() {
        Session session = null;
        List<Translate> t = null;
        try {
            session = sessionFactory.openSession();
            session.beginTransaction();
            t = session.createCriteria(Translate.class).list();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return t;
    }

    @Override
    public List<Translate> getAll(List<Verb> verbsIds, List<Prefix> prefixIds) {
        Session session = sessionFactory.openSession();
        String query = "From Translate as t where t.verb in (:verbs) and t.prefix in (:prefixes)";
        Query q = session.createQuery(query).setParameterList("verbs", verbsIds).setParameterList("prefixes", prefixIds);
        List<Translate> translates = q.list();
        session.close();
        return translates;

    }

    @Override
    public void remove(Translate t) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(t);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(Translate t) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.update(t);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public Translate getTranslateWithJoins(int id) {
        Session s = sessionFactory.openSession();
        Translate t = (Translate) s.get(Translate.class, id);
        //Hibernate.initialized(getVerbs(t));
        return t;
    }
    
}
