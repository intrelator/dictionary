/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Interfaces;

import Entities.Verb;
import java.util.List;

/**
 *
 * @author root
 */
public interface VerbInterface {
    public void add(Verb v);
    public List<Verb> getAll();
    public void remove(Verb v);
    public void update(Verb v);
    public Verb getByTitle(String t);
    public Verb getVerbWithJoins(int id);
}
