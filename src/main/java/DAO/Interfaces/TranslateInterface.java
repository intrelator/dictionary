/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Interfaces;

import Entities.Prefix;
import Entities.Translate;
import Entities.Verb;
import java.util.List;

/**
 *
 * @author root
 */
public interface TranslateInterface {
    public void add(Translate t);
    public List<Translate> getAll();
    public List<Translate> getAll(List<Verb> verbsIds, List<Prefix> prefixIds);
    public Translate getTranslateWithJoins(int id);
    public void remove(Translate t);
    public void update(Translate t);
}
