/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Interfaces;

import Entities.Prefix;
import java.util.List;

/**
 *
 * @author root
 */
public interface PrefixInterface {
    public void add(Prefix p);
    public List<Prefix> getAll();
    public Prefix getPrefixWithJoins(int id);
    public void remove(Prefix p);
    public void update(Prefix p);
    public Prefix getByTitle(String t);
}
