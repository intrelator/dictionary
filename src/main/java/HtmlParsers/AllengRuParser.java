package HtmlParsers;

import Model.addTrFromAjax;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class AllengRuParser {
    private List<addTrFromAjax> list = new ArrayList<>();
    private final static String urlPath = "http://www.alleng.ru/mybook/7phv170/TOP170_alph1.htm";
    public List<addTrFromAjax> parse() throws IOException {
        List<String[]> verbTr = new ArrayList<>();
        Document doc;
        doc = Jsoup.connect(urlPath).get();
        Element table = doc.getElementById("table140");
        // verbs parsing
        Elements tableRows = table.children().get(0).children();
        for (int i = 1; i < tableRows.size(); i++) {
            Element e = tableRows.get(i);
            Element verb = e.child(1);
            String[] vTr = {"", ""};
            Elements a = verb.getElementsByTag("a");
            if (a.get(0).children().size() < 3) {
                vTr[0] = a.get(0).child(0).ownText();
            }
            if (a.get(0).children().size() > 2) {
                vTr[0] = a.get(0).child(0).child(0).ownText();
                vTr[0] = vTr[0] + " " + a.get(0).child(2).child(0).ownText();
            }
            //translates parsing
            Element tr = e.child(2);
            Elements spans = tr.getElementsByTag("span");
            String innerText = "";
            for (Element el : spans) {
                Elements italic = el.getElementsByTag("i");
                if (italic.size() == 0) {
                    vTr[1] = vTr[1] + " " + el.ownText() + "++";
                }

                for (Element element : italic) {
                    vTr[1] = vTr[1] + " " + element.ownText() + "++";
                }

            }

            list.add(convertToEntity(vTr));
            //System.out.println(vTr[0]+"      "+vTr[1]);
        }
        return list;

    }

    private addTrFromAjax convertToEntity(String[] verbTranslate) {        
        
            String[] verbStrings = verbTranslate[0].split(" ");
            String[] translateStrings = verbTranslate[1].split("\\+\\+");
            StringBuilder sb = new StringBuilder();
            for (String s : translateStrings) {
                String tmp = cleanText(s);
                if (tmp.length() < 4) {
                    continue;
                }
                sb.append(tmp);
                sb.append(";");
            }
            addTrFromAjax t = new addTrFromAjax();
            t.setTranslate(sb.toString());
            t.setVerb(verbStrings[0]);
            t.setPrefix(verbStrings[1]);
            return t;
            //System.out.println(verbStrings[0] + " " + verbStrings[1] + " " + sb.toString());
            
            
        
    }

    private String cleanText(String s) {
//        if(s.contains("(")||s.contains(")")){
//            
//        }
        Pattern p = Pattern.compile("[а-я]*(\\s?,?;?[а-я]*)*");
        Matcher m = p.matcher(s);
        m.find();
        s = m.group();

        return s;
    }
}
