/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 *
 * @author root
 */
@Entity
@Table(name="prefix")
public class Prefix {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)    
    private int idPrefix;
    @Column
    private String title;
    @OneToMany(mappedBy = "prefix")
    Set<Translate> translates = new HashSet<Translate>();
    public int getIdPrefix() {
        return idPrefix;
    }

    public void setIdPrefix(int idPrefix) {
        this.idPrefix = idPrefix;
    }

    public String getTitle() {
        return title;
    }

    public Set<Translate> getTranslates() {
        return translates;
    }

    public void setTranslates(Set<Translate> translates) {
        this.translates = translates;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
}
