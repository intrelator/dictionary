/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author root
 */
@Entity
@Table(name = "verb")
public class Verb implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idverb")
    private int idVerb;
    @Column
    private String title;
//    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
//    @JoinTable(name = "VerbHasPrefix",
//            joinColumns = @JoinColumn(name = "verb_idverb", referencedColumnName = "idVerb"),
//            inverseJoinColumns = @JoinColumn(name = "prefix_idprefix", referencedColumnName = "idPrefix"))
    @OneToMany(mappedBy = "verb")
    Set<Translate> translates = new HashSet<Translate>();
    public int getIdVerb() {
        return idVerb;
    }

    public void setIdVerb(int idVerb) {
        this.idVerb = idVerb;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Translate> getTranslates() {
        return translates;
    }

    public void setTranslates(Set<Translate> translates) {
        this.translates = translates;
    }

}
