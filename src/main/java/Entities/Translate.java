/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;
import javax.persistence.*;
/**
 *
 * @author root
 */
@Entity
@Table(name="translate")
public class Translate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idtranslate")
    private int idTranslate;
    @Column(length = 1000)
    private String title;
    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "idverb")
    private Verb verb;
    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "idprefix")
    private Prefix prefix;
    @Column(columnDefinition = "int default 0")
    private int shows;
    @Column(columnDefinition = "int default 0")
    private int hints;
    @Column
    private String usecase;
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(getIdTranslate());
        sb.append("  ");
        sb.append(getTitle());
        sb.append("  ");
        sb.append(getVerb());
        sb.append("  ");
        sb.append(getPrefix());
        return sb.toString();
        
    }
    public int getIdTranslate() {
        return idTranslate;
    }

    public void setIdTranslate(int idTranslate) {
        this.idTranslate = idTranslate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Verb getVerb() {
        return verb;
    }

    public void setVerb(Verb verb) {
        this.verb = verb;
    }

    public Prefix getPrefix() {
        return prefix;
    }

    public void setPrefix(Prefix prefix) {
        this.prefix = prefix;
    }

    public int getShows() {
        return shows;
    }

    public void setShows(int shows) {
        this.shows = shows;
    }

    public int getHints() {
        return hints;
    }

    public void setHints(int hints) {
        this.hints = hints;
    }

    public String getUsecase() {
        return usecase;
    }

    public void setUsecase(String usecase) {
        this.usecase = usecase;
    }
    
}
