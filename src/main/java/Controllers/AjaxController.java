/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import DAO.Interfaces.PrefixInterface;
import DAO.Interfaces.TranslateInterface;
import DAO.Interfaces.VerbInterface;
import Entities.Translate;
import Model.RecivedAnswer;
import Model.ResponseBodyAddTranslate;
import Model.ResponseTranslate;
import Model.addTrFromAjax;
import Services.DictionaryServiceInterface;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author root
 */
@Controller
public class AjaxController {

    @Autowired
    VerbInterface verbDao;
    @Autowired
    PrefixInterface prefixDao;
    @Autowired
    TranslateInterface translateDao;
    @Autowired
    DictionaryServiceInterface dictionaryService;

    @RequestMapping(value = "getAllTranslates")
    @ResponseBody
    public ResponseTranslate[] getAllTranslates(/*@RequestParam("request") String s*/) {
        ResponseTranslate[] response;
        List<Translate> list;
        list = translateDao.getAll();
        response = new ResponseTranslate[list.size()];
        
        for (int i = 0; i < list.size(); i++) {
            
            response[i]= new ResponseTranslate();
            response[i].setIdTranslate(list.get(i).getIdTranslate());
            response[i].setPrefix(list.get(i).getPrefix().getTitle());
            response[i].setVerb(list.get(i).getVerb().getTitle());
            response[i].setTitle(list.get(i).getTitle());
        }

        return response;
    }
    @RequestMapping(value="editTranslate")
    @ResponseBody
    public RecivedAnswer editTranslate(@RequestBody ResponseTranslate requestBody){
        RecivedAnswer response = new RecivedAnswer();
        dictionaryService.updateTranslate(requestBody);
        return response;
    }
    
    //@JsonView(Views.Public.class)

    @RequestMapping(value = "createTranslate")
    @ResponseBody
    public ResponseBodyAddTranslate createTranslate(@RequestBody addTrFromAjax translate) {
        dictionaryService.addTranslate(translate);
        
        ResponseBodyAddTranslate resp = new ResponseBodyAddTranslate();
        resp.setMessage("Added");
        return resp;
    }
    //testing lazy fetch
    @ResponseBody
    @RequestMapping(value = "getVerbbyid/{id}")
    
    public String gettingAnswerSendingQuestion(@PathVariable int id) {
        
        return verbDao.getVerbWithJoins(id).getTranslates().toString();
    }
}
