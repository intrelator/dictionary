package Controllers;

public class Resp {
    private int idverb;
    private String verb;
    private int idprefix;
    private String prefix;
    private String[] translates;

    public String[] getTranslates() {
        return this.translates;
    }

    public void setTranslates(String[] translates) {
        this.translates = translates;
    }

    public void setIdverb(int idverb) {
        this.idverb = idverb;
    }

    public void setVerb(String verb) {
        this.verb = verb;
    }

    public void setIdprefix(int idprefix) {
        this.idprefix = idprefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public int getIdverb() {
        return this.idverb;
    }

    public String getVerb() {
        return this.verb;
    }

    public int getIdprefix() {
        return this.idprefix;
    }

    public String getPrefix() {
        return this.prefix;
    }
}