/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import HtmlParsers.AllengRuParser;
import Model.addTrFromAjax;
import Services.DictionaryServiceInterface;
import com.fasterxml.jackson.annotation.JsonView;
import java.util.List;
import java.util.ListIterator;
import javax.servlet.http.HttpServletRequest;
import jsonView.Views;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author root
 */
@Controller
public class General {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
   DictionaryServiceInterface dictionaryService;

    @RequestMapping("/")
    public ModelAndView welcome() throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
//        AllengRuParser parser = new AllengRuParser();
//        List<addTrFromAjax> translateList = parser.parse();
//        for(addTrFromAjax t : translateList){
//            dictionaryService.addTranslate(t);
//        }
        
        return modelAndView;
    }
    
    @JsonView(Views.Public.class)
    @RequestMapping(value = "/reateTranslate")
    @ResponseBody
    public Resp ajaxTest(@RequestBody addTrFromAjax translate, HttpServletRequest request) {
        Resp resp = new Resp();
        String s = request.getParameter("verb");
        resp.setVerb(s);
        return resp;
    }

    @RequestMapping(value = "getVerb/{request}")
    public Resp ajaxGetVerb(@RequestParam String request) {
        Resp resp = new Resp();
        resp.setIdprefix(1);
        return resp;
    }

    @RequestMapping(value = "dictionary")
    public ModelAndView dictionary() {
        ModelAndView modelAndView = new ModelAndView("dictionary");
        return modelAndView;
    }
}
