package Controllers;

import Entities.Translate;
import Model.QuestionUnit;
import Model.RecivedAnswer;
import Model.RespForGetQuestion;
import Model.RespForShowResult;
import Services.DictionaryServiceInterface;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TrainingController {

    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    DictionaryServiceInterface dictionaryService;

    @RequestMapping(value = {"training"})
    public ModelAndView doTraining() {
        ModelAndView modelAndView = new ModelAndView("training");
        modelAndView.addObject("prefixes", (Object) this.dictionaryService.getAllPrefixes());
        modelAndView.addObject("verbs", (Object) this.dictionaryService.getAllVerbs());
        return modelAndView;
    }

    @RequestMapping(value = {"setVerbsAndPrefix"}, method = {RequestMethod.POST})
    @ResponseBody
    public RespForGetQuestion ajaxTest(HttpServletRequest request) {
        RespForGetQuestion resp = new RespForGetQuestion();
        HttpSession httpSession = request.getSession();
        String verbIds = request.getParameter("verbIds");
        String prefixIds = request.getParameter("prefixIds");
        List<Translate> translateUnit = this.dictionaryService.getWordSetFallingUnderFilter(verbIds, prefixIds);
        QuestionUnit questionUnit = this.dictionaryService.getQuestionUnit(translateUnit);
        int shows = translateUnit.get(questionUnit.getCorrectAnswerId()).getShows();
        translateUnit.get(questionUnit.getCorrectAnswerId()).setShows(dictionaryService.incrementShows(shows));
        httpSession.setAttribute("translateUnit", (Object) translateUnit);
        httpSession.setAttribute("shows", (Object) 1);
        httpSession.setAttribute("hints", (Object) 0);
        httpSession.setAttribute("answerId", questionUnit.getCorrectAnswerId());
        resp.setPrefix(translateUnit.get(questionUnit.getCorrectAnswerId()).getPrefix().getTitle());
        resp.setVerb(translateUnit.get(questionUnit.getCorrectAnswerId()).getVerb().getTitle());
        resp.setTranslates(questionUnit.getAnswers());
        resp.setTranslatesId(questionUnit.getAnswersId());
        return resp;
    }

    @ResponseBody
    @RequestMapping(value = {"getNewQuestion"}, method = {RequestMethod.POST})
    public RespForGetQuestion ajaxGetQuestion(@RequestBody RecivedAnswer answer, HttpServletRequest request) {
        List<Translate> translateUnit = (List) request.getSession().getAttribute("translateUnit");
        int answerId = (int) request.getSession().getAttribute("answerId");
        boolean result = this.dictionaryService.checkAnswer(answer.getAnswer(), answerId);
        if (result) {
            int hints = translateUnit.get(answerId).getHints();
            translateUnit.get(answerId).setHints(dictionaryService.incrementHints(hints));
        }
        QuestionUnit questionUnit = this.dictionaryService.getQuestionUnit(translateUnit);
        int shows = translateUnit.get(questionUnit.getCorrectAnswerId()).getShows();
        translateUnit.get(questionUnit.getCorrectAnswerId()).setShows(dictionaryService.incrementShows(shows));
        int show = (Integer) request.getSession().getAttribute("shows");
        request.getSession().setAttribute("shows", (Object) (++show));
        RespForGetQuestion resp = new RespForGetQuestion();
        resp.setPrefix(translateUnit.get(questionUnit.getCorrectAnswerId()).getPrefix().getTitle());
        resp.setVerb(translateUnit.get(questionUnit.getCorrectAnswerId()).getVerb().getTitle());
        resp.setTranslates(questionUnit.getAnswers());
        resp.setTranslatesId(questionUnit.getAnswersId());
        resp.setResult(result);
        return resp;
    }

    @RequestMapping(value = {"stopTraining"}, method = {RequestMethod.POST})
    @ResponseBody
    public RespForShowResult showResult(HttpServletRequest request) {
        RespForShowResult resp = new RespForShowResult();
        int shows = (Integer) request.getSession().getAttribute("shows");
        int hints = (Integer) request.getSession().getAttribute("hints");
        List translate = (List) request.getSession().getAttribute("translateUnit");
        this.dictionaryService.updateStatistics(translate);
        int combinations = translate.size();
        resp.setShows(shows);
        resp.setHints(hints);
        resp.setCombinationsAviable(combinations);
        return resp;
    }
}
