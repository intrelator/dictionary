/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import DAO.Interfaces.PrefixInterface;
import DAO.Interfaces.TranslateInterface;
import DAO.Interfaces.VerbInterface;
import Entities.Prefix;
import Entities.Translate;
import Entities.Verb;
import Model.QuestionUnit;
import Model.ResponseTranslate;
import Model.addTrFromAjax;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author root
 */
@Service
public class DictionaryService implements DictionaryServiceInterface {

    @Autowired
    VerbInterface verbDao;
    @Autowired
    PrefixInterface prefixDao;
    @Autowired
    TranslateInterface translateDao;

    @Override
    public QuestionUnit getQuestionUnit(List<Translate> translateUnit) {
        QuestionUnit questionUnit = new QuestionUnit();        
        String[] answers = new String[4];
        int[] answersId = new int[4];
        int correctId = getRand(translateUnit.size());
        questionUnit.setCorrectAnswerId(correctId);
        questionUnit.setCorrectAnswer(translateUnit.get(correctId).getTitle());
        int[] randIndexForWrongTr = getMasOfRand(translateUnit.size(), 3);
        int[] randIndexForAnswers = getMasOfRand(4, 4);
        for (int i = 0; i < 3; i++) {
            answers[randIndexForAnswers[i]] = translateUnit.get(randIndexForWrongTr[i]).getTitle().split(";")[0];
            answersId[randIndexForAnswers[i]] = randIndexForWrongTr[i];
        }
        for (int i = 0; i < answers.length; i++) {
            if (answers[i] == (null)) {
                answers[i] = questionUnit.getCorrectAnswer().split(";")[0];
                answersId[i] = correctId;
                break;
            }
        }
        questionUnit.setAnswers(answers);
        questionUnit.setAnswersId(answersId);
        return questionUnit;
    }

    @Override
    public List<Translate> getWordSetFallingUnderFilter(String parameterVerbIds,String parameterPrefixIds) {
        String[] requestParameterVerbIds = parameterVerbIds.split(";");
        String[] requestParameterPrefixIds = parameterPrefixIds.split(";");
        List<Verb> verbIds = new ArrayList<>();
        for (int i = 0; i < requestParameterVerbIds.length; i++) {
            Verb v = new Verb();
            int id = Integer.parseInt(requestParameterVerbIds[i]);
            v.setIdVerb(id);
            verbIds.add(v);
        }
        List<Prefix> prefixesIds = new ArrayList<>();
        for (int i = 0; i < requestParameterPrefixIds.length; i++) {
            Prefix p = new Prefix();
            int id = Integer.parseInt(requestParameterPrefixIds[i]);
            p.setIdPrefix(id);
            prefixesIds.add(p);

        }
        List<Translate> translates;
        translates = translateDao.getAll(verbIds, prefixesIds);
        return translates;
    }

    private int[] getMasOfRand(int bound, int masSize) {
        List<Integer> list = new ArrayList<Integer>();
        while (list.size() != masSize) {
            int r = getRand(bound);
            boolean listDoesntContainsTheSameValue = true;
            for (int i = 0; i < list.size(); i++) {
                if (r == list.get(i)) {
                    listDoesntContainsTheSameValue = false;
                    break;
                }
            }
            if (listDoesntContainsTheSameValue) {
                list.add(r);
            }
        }
        int[] mas = new int[masSize];
        for (int i = 0; i < mas.length; i++) {
            mas[i] = list.get(i);
        }
        return mas;
    }

    private int getRand(int bound) {
        int rand = new Random().nextInt(bound);
        return rand;
    }

    @Override
    public List<Verb> getAllVerbs() {
        return verbDao.getAll();

    }

    @Override
    public List<Prefix> getAllPrefixes() {
        return prefixDao.getAll();
    }

    @Override
    public boolean checkAnswer(int answer, int answerId) {        
        boolean result = false;
        if (answer == answerId) {
            result=true;
        }

        return result;
    }

    @Override
    public void updateStatistics(List<Translate> tr) {
        for (Translate t : tr) {
            translateDao.update(t);
        }
    }

    @Override
    public void updateTranslate(ResponseTranslate t) {
        Verb verb = verbDao.getByTitle(t.getVerb());
        if (verb == null) {
            verb = new Verb();
            verb.setTitle(t.getVerb());
            verbDao.add(verb);
        }
        Prefix prefix = prefixDao.getByTitle(t.getPrefix());
        if (prefix == null) {
            prefix = new Prefix();
            prefix.setTitle(t.getPrefix());
        }
        Translate tr = new Translate();
        tr.setIdTranslate(t.getIdTranslate());
        tr.setPrefix(prefix);
        tr.setVerb(verb);
        tr.setTitle(t.getTitle());
        tr.setUsecase(t.getUsecase());
        translateDao.update(tr);
    }

    @Override
    public void addTranslate(addTrFromAjax translate) {

        Verb verb = verbDao.getByTitle(translate.getVerb());
        if (verb == null) {
            verb = new Verb();
            verb.setTitle(translate.getVerb());
            verbDao.add(verb);
        }
        Prefix prefix = prefixDao.getByTitle(translate.getPrefix());
        if (prefix == null) {
            prefix = new Prefix();
            prefix.setTitle(translate.getPrefix());
        }
        Translate t = new Translate();
        t.setPrefix(prefix);
        t.setVerb(verb);
        t.setTitle(translate.getTranslate());
        t.setUsecase(translate.getExample());
        translateDao.add(t);
    }

    @Override
    public int incrementShows(int shows) {
        return ++shows;
    }

    @Override
    public int incrementHints(int hints) {
        return ++hints;
    }
    
    

}
