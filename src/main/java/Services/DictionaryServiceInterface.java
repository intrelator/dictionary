/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entities.Prefix;
import Entities.Translate;
import Entities.Verb;
import Model.QuestionUnit;
import Model.ResponseTranslate;
import Model.addTrFromAjax;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author root
 */
public interface DictionaryServiceInterface {
    public QuestionUnit getQuestionUnit(List<Translate> translateUnit);
    public int incrementShows(int shows);
    public int incrementHints(int hints);
    public List<Translate> getWordSetFallingUnderFilter(String parameterVerbIds,String parameterPrefixIds);
    public List<Verb> getAllVerbs();
    public List<Prefix> getAllPrefixes();
    public boolean checkAnswer(int answer, int answerId);
    public void updateStatistics(List<Translate> tr);
    public void updateTranslate(ResponseTranslate t);
    public void addTranslate(addTrFromAjax translate);
    
}
